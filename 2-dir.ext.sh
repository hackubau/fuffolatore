#!/bin/bash

cd /hack-lists/SecLists && git reset --hard HEAD && git fetch && git pull

export directory=hack-lists/SecLists/Discovery/Web-Content/directory-list-2.3-small.txt
export extensions=hack-lists/SecLists/Discovery/Web-Content/web-extensions.txt
export extensionsFuffe=hack-lists/SecLists/Discovery/Web-Content/raft-medium-extensions.txt
export parameters=hack-lists/SecLists/Discovery/Web-Content/burp-parameter-names.txt

export host=10.10.10.46
export port=80
export protocol=http

echo cleaning seclists file
sed -i 's/^#.*$//g' $directory
sed -i '/^$/d' $directory

tr "\n"

rm 2-dir.ext.out
for EXT in `uniq 1-index.ext.out | sed '/^$/d'`
do
  echo "searching directory/files with extension: $EXT using autocalibration"
	ffuf -w $directory -u $protocol://$host:$port/FUZZ -e "$EXT" -recursion -recursion-depth 2 -s -ac >> 2-dir.ext.out
	echo finish
done

#ONE COMMAND LINE VERSION:
#ffuf -w $extensions -u $protocol://$host:$port/indexFUZZ -s | ffuf -w $directory -u $protocol://$host:$port/FUZZ -e "$(cat)" -recursion -recursion-depth 2

