#!/bin/bash

cd /hack-lists/SecLists && git reset --hard HEAD && git fetch && git pull

export directory=hack-lists/SecLists/Discovery/Web-Content/directory-list-2.3-small.txt
export extensions=hack-lists/SecLists/Discovery/Web-Content/web-extensions.txt
export extensionsFuffe=hack-lists/SecLists/Discovery/Web-Content/raft-medium-extensions.txt
export parameters=hack-lists/SecLists/Discovery/Web-Content/burp-parameter-names.txt

export host=10.10.10.46
export port=80
export protocol=http

echo cleaning seclists file:
sed -i 's/^#.*$//g' $directory
sed -i '/^$/d' $directory

echo searching extension by searching index page extensions
rm 1-index.ext.out
ffuf -w $extensions -u $protocol://$host:$port/indexFUZZ -s >> 1-index.ext.out
sed -i '/^$/d' 1-index.ext.out