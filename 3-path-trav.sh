#!/bin/bash

cd /hack-lists/SecLists && git reset --hard HEAD && git fetch && git pull

export directory=hack-lists/SecLists/Discovery/Web-Content/directory-list-2.3-small.txt
export extensions=hack-lists/SecLists/Discovery/Web-Content/web-extensions.txt
export extensionsFuffe=hack-lists/SecLists/Discovery/Web-Content/raft-medium-extensions.txt
export parameters=hack-lists/SecLists/Discovery/Web-Content/burp-parameter-names.txt
#export dirTraversal=hack-lists/SecLists/Fuzzing/LFI/LFI-LFISuite-pathtotest.txt
export dirTraversal=hack-lists/SecLists/Fuzzing/LFI/LFI-Jhaddix.txt

export host=10.10.10.46
export port=80
export protocol=http

echo cleaning seclists file
sed -i 's/^#.*$//g' $directory
sed -i '/^$/d' $directory

tr "\n"

rm 3-path-trav.out
echo check traverse path
ffuf -w $dirTraversal -u $protocol://$host:$port/FUZZ -ac -s >> 3-path-trav.out
echo finish

#ONE COMMAND LINE VERSION:
#ffuf -w $extensions -u $protocol://$host:$port/indexFUZZ -s | ffuf -w $directory -u $protocol://$host:$port/FUZZ -e "$(cat)" -recursion -recursion-depth 2

